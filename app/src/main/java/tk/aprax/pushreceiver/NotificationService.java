package tk.aprax.pushreceiver;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.Settings;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.service.notification.StatusBarNotification;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.gson.GsonBuilder;
import com.nextcloud.android.sso.AccountImporter;
import com.nextcloud.android.sso.aidl.NextcloudRequest;
import com.nextcloud.android.sso.api.NextcloudAPI;
import com.nextcloud.android.sso.model.SingleSignOnAccount;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.crypto.Cipher;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSourceListener;
import okhttp3.sse.EventSources;

public class NotificationService extends Service {
    private static final String LOG_TAG = "NC-NotificationService";

    private NextcloudAPI nextcloudAPI = null;
    private EventSource source = null;
    private EventSource.Factory factory;
    private PrivateKey privateKey;
    private NotificationManagerCompat notificationManager;
    private final String GROUP_KEY_NOTIFICATION = "tk.aprax.pushreceiver.NOTIFICATION";
    private final String messageChannelId = "tk_aprax_pushreceiver_01";
    private final String serviceChannelId = "tk_aprax_pushreceiver_s";
    private final String warningChannelId = "tk_aprax_pushreceiver_w";
    private Request request;
    private OkHttpClient client;
    public long pingtime;
    private boolean gracefulviolence = false;
    private boolean started = false;
    private boolean networkConnected = false;
    private static SharedPreferences mPref;
    private Map<String, String> accountIdentifiers = new HashMap<>();
    private Map<String, Integer> accountNumbers = new HashMap<>();

    PowerManager.WakeLock wakeLock;

    public static String encryptString(String value, PublicKey publicKey){
        byte[] encodedBytes = null;
        try {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            encodedBytes = cipher.doFinal(value.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Base64.encodeToString(encodedBytes, Base64.DEFAULT);
    }

    public static String decryptString(String value, PrivateKey privateKey){
        byte[] decodedBytes = null;
        try {
            Cipher c = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            c.init(Cipher.DECRYPT_MODE, privateKey);
            decodedBytes = c.doFinal(Base64.decode(value, Base64.DEFAULT));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new String(decodedBytes);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // doesn't run
        Log.d(LOG_TAG, "Running onBind");
        return null;
    }

    @Override
    public void onCreate() {
        // runs first
        super.onCreate();
        Log.d(LOG_TAG, "Running onCreate");

        client = new OkHttpClient.Builder()
                .readTimeout(0, TimeUnit.SECONDS)
                .retryOnConnectionFailure(false)
                .build();

        notificationManager = NotificationManagerCompat.from(this);

        NotificationChannel mChannel = new NotificationChannel(messageChannelId, "Nextcloud Message", NotificationManager.IMPORTANCE_HIGH);
        mChannel.setDescription("Push messages from Nextcloud");
        mChannel.enableLights(true);
        mChannel.setLightColor(Color.RED);
        mChannel.enableVibration(true);
        mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        notificationManager.createNotificationChannel(mChannel);

        mChannel = new NotificationChannel(serviceChannelId, "Nextcloud Service", NotificationManager.IMPORTANCE_NONE);
        mChannel.setDescription("Nextcloud Notification Service");
        notificationManager.createNotificationChannel(mChannel);

        mChannel = new NotificationChannel(warningChannelId, "Nextcloud Warning", NotificationManager.IMPORTANCE_HIGH);
        mChannel.setDescription("Push Messaging WARNING");
        notificationManager.createNotificationChannel(mChannel);

        Notification newNotification =
                new NotificationCompat.Builder(NotificationService.this, serviceChannelId)
                        .setSmallIcon(android.R.drawable.ic_input_add)
                        .setContentTitle("Nextcloud-Service")
                        .setContentText("Keep connection alive")
                        .build();

        wakeLock = ((PowerManager)getSystemService(POWER_SERVICE)).newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "tk.aprax.pushreceiver:wakelock");

        registerReceiver(receiver, new IntentFilter("tk.aprax.pushreceiver.NOTIFICATION_DELETED"));

        startForeground(12345, newNotification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // runs second
        Log.d(LOG_TAG, "Running onStartCommand");

        mPref = getSharedPreferences("config", MODE_PRIVATE);

        if (!started){
            registerNetworkCallback();
            started = true;
        } else if (networkConnected && intent.hasExtra("force")){
            if (source != null){
                gracefulviolence = true;
                source.cancel();
            }
            new Thread(this::doConnect).start();
        } else if (networkConnected){
            // This is a refresh start.
            // If we haven't received any messages in the last 10 minutes, reset the connection.
            Log.d(LOG_TAG, "REFRESH, pingtime: " + pingtime + ", currentTime: " + System.currentTimeMillis());
            if (pingtime + 10 * 60 * 1000 < System.currentTimeMillis() && source != null){
                wakeLock.acquire(5 * 1000L); // 5 seconds
                gracefulviolence = true;
                source.cancel(); // this will most likely trigger listener.onFailure()
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                source = factory.newEventSource(request, listener);
            }
        }

        return START_STICKY;
    }

    NextcloudAPI.ApiConnectedListener callback = new NextcloudAPI.ApiConnectedListener() {
        @Override
        public void onConnected() {
            Log.d(LOG_TAG, "Nextcloud onConnected");
        }

        @Override
        public void onError(Exception e) {
            e.printStackTrace();
        }
    };

    public void registerNetworkCallback(){
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) NotificationService.this.getSystemService(Context.CONNECTIVITY_SERVICE);
            connectivityManager.registerDefaultNetworkCallback(new ConnectivityManager.NetworkCallback(){
                @Override
                public void onAvailable(Network network) {
                    Log.d(LOG_TAG, "Network is CONNECTED");
                    if (!networkConnected) {
                        networkConnected = true;
                        new Thread(NotificationService.this::doConnect).start();
                    }
                }
                @Override
                public void onLost(Network network) {
                    Log.d(LOG_TAG, "Network is DISCONNECTED");
                    networkConnected = false;
                    if (source != null){
                        gracefulviolence = true;
                        source.cancel();
                    }
                }
            });
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    void doConnect(){
        KeyPair pair = null;

        try {
            KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
            ks.load(null);
            if (ks.containsAlias("tk.aprax.pushreceiver")) {
                Key key = ks.getKey("tk.aprax.pushreceiver", null);
                if (key instanceof PrivateKey) {
                    Certificate cert = ks.getCertificate("tk.aprax.pushreceiver");
                    pair = new KeyPair(cert.getPublicKey(), (PrivateKey) key);
                    Log.d(LOG_TAG, "Retrieved keys");
                }
            } else {
                KeyPairGenerator kpg = KeyPairGenerator.getInstance(
                        KeyProperties.KEY_ALGORITHM_RSA, "AndroidKeyStore");

                kpg.initialize(new KeyGenParameterSpec.Builder(
                        "tk.aprax.pushreceiver",
                        KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
                        .setDigests(KeyProperties.DIGEST_SHA512)
                        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_RSA_PKCS1)
                        .setKeySize(2048)
                        .build());

                pair = kpg.generateKeyPair();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (pair == null) return;

        String pubkey = "-----BEGIN PUBLIC KEY-----\n" + Base64.encodeToString(pair.getPublic().getEncoded(), Base64.NO_WRAP).replaceAll("(.{64})", "$1\n") + "\n-----END PUBLIC KEY-----";
        privateKey = pair.getPrivate();

        // Register for push messages (send to NEXTCLOUD)
        // curl -u username:appPassword --user-agent "Mozilla/5.0 (Android) Nextcloud-Talk v11" -X POST --header "OCS-APIRequest: true" 'https://your.nextcloud.inst/ocs/v2.php/apps/notifications/api/v2/push?devicePublicKey={...}&proxyServer={...}&pushTokenHash={...}'

        // SSE connection (send to push proxy)
        // curl --request POST --data 'deviceIdentifier={...}&pushTokenHash={...}' https://your.nextcloud.inst/pushclient.php

        @SuppressLint("HardwareIds") String androidid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        StringBuilder sb = new StringBuilder();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            byte[] data = md.digest(androidid.getBytes());
            for (byte datum : data) {
                sb.append(Integer.toString((datum & 0xff) + 0x100, 16).substring(1));
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        String pushTokenHash = sb.toString();
        String deviceIdentifier = null;
        accountIdentifiers.clear();
        accountNumbers.clear();
        int cAcctNum = 0;

        Map<String, String> parameters = new HashMap<>();
        parameters.put("devicePublicKey", pubkey);
        String proxyURI = mPref.getString("proxy", null);
        if (proxyURI == null) return;
        Log.d(LOG_TAG, "Proxy: " + proxyURI);
        parameters.put("proxyServer", proxyURI);
        parameters.put("pushTokenHash", pushTokenHash);
        parameters.put("format", "json");

        NextcloudRequest nextcloudRequest = new NextcloudRequest.Builder()
                .setMethod("POST")
                .setParameter(parameters)
                .setUrl("/ocs/v2.php/apps/notifications/api/v2/push")
                .build();

        StringBuilder deviceIdentifierList = new StringBuilder();
        Set<String> storedAccounts = mPref.getStringSet("accounts", new HashSet<>());
        for (String account : storedAccounts){
            try {
                SingleSignOnAccount ssoAccount = AccountImporter.getSingleSignOnAccount(this, account);
                nextcloudAPI = new NextcloudAPI(this, ssoAccount, new GsonBuilder().create(), callback);
            } catch (Exception e){
                e.printStackTrace();
            }
            byte[] databuff = new byte[1024];
            StringBuilder apiresponse = new StringBuilder();
            try {
                InputStream inputStream = nextcloudAPI.performNetworkRequest(nextcloudRequest);
                int rlen;
                while ((rlen = inputStream.read(databuff)) >= 0)
                    apiresponse.append(new String(databuff, 0, rlen));
                Log.d(LOG_TAG, "Registration response: " + apiresponse);
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                JSONObject devreg = new JSONObject(apiresponse.toString());
                JSONObject ocs = devreg.getJSONObject("ocs");
                JSONObject data = ocs.getJSONObject("data");
                deviceIdentifier = data.getString("deviceIdentifier");
                Log.d(LOG_TAG, "Device Identifier: " + deviceIdentifier);
            } catch (Exception e) {
                e.printStackTrace();
                deviceIdentifier = null;
            }
            if (deviceIdentifier == null) return;

            accountIdentifiers.put(deviceIdentifier, account);
            accountNumbers.put(account, cAcctNum++);
            deviceIdentifierList.append(deviceIdentifier+",");
        }
        deviceIdentifierList.deleteCharAt(deviceIdentifierList.length()-1);

        // Perform authorization request.
        parameters = new HashMap<>();
        parameters.put("deviceIdentifiers", deviceIdentifierList.toString());
        parameters.put("pushTokenHash", pushTokenHash);
        parameters.put("format", "json");

        nextcloudRequest = new NextcloudRequest.Builder()
                .setMethod("POST")
                .setParameter(parameters)
                .setUrl("/index.php/apps/ssepush/authorize")
                .build();

        try {
            SingleSignOnAccount ssoAccount =
                    AccountImporter.getSingleSignOnAccount(this, mPref.getString("master", null));
            nextcloudAPI = new NextcloudAPI(this, ssoAccount, new GsonBuilder().create(), callback);
        } catch (Exception e){
            e.printStackTrace();
        }

        StringBuilder apiresponse = new StringBuilder();
        try {
            byte[] databuff = new byte[1024];
            InputStream inputStream = nextcloudAPI.performNetworkRequest(nextcloudRequest);
            int rlen;
            while ((rlen = inputStream.read(databuff)) >= 0)
                apiresponse.append(new String(databuff, 0, rlen));
            Log.d(LOG_TAG, "Authorization response: " + apiresponse);
            inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String domain = mPref.getString("sse", null);
        if (domain == null) return;
        RequestBody postBody = new FormBody.Builder()
                .add("deviceIdentifiers", deviceIdentifierList.toString())
                .add("pushTokenHash", pushTokenHash)
                .build();
        request = new Request.Builder().url(domain)
                .post(postBody)
                .build();

        factory = EventSources.createFactory(client);
        source = factory.newEventSource(request, listener);
        Log.d(LOG_TAG, "doConnect done.");
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent mintent) {
            Log.d(LOG_TAG, "Notification deleted");

            if(mintent.hasExtra("account") && mintent.hasExtra("nid")){
                new Thread(() -> {
                    Log.d(LOG_TAG, "Extras -- account: "+mintent.getStringExtra("account")+", nid: "+mintent.getStringExtra("nid"));
                    try {
                        NextcloudRequest nextcloudRequest = new NextcloudRequest.Builder()
                                .setMethod("DELETE")
                                .setUrl("/ocs/v2.php/apps/notifications/api/v2/notifications/" + mintent.getStringExtra("nid"))
                                .build();

                        SingleSignOnAccount ssoAccount =
                                AccountImporter.getSingleSignOnAccount(context, mintent.getStringExtra("account"));
                        nextcloudAPI = new NextcloudAPI(context, ssoAccount, new GsonBuilder().create(), callback);

                        byte[] databuff = new byte[1024];
                        InputStream inputStream = nextcloudAPI.performNetworkRequest(nextcloudRequest);
                        int rlen;
                        StringBuilder apiresponse = new StringBuilder();
                        while ((rlen = inputStream.read(databuff)) >= 0)
                            apiresponse.append(new String(databuff, 0, rlen));
                        Log.d(LOG_TAG, "Notifications DELETE response: " + apiresponse);
                        inputStream.close();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }).start();
            }
        }
    };

    EventSourceListener listener = new EventSourceListener() {

        void doNotify(String application, String account, String type, String subject, String nid){
            wakeLock.acquire(5 * 1000L); // 5 seconds
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.nextcloud.talk2", "com.nextcloud.talk.activities.MainActivity"));
            PendingIntent pendIntent = PendingIntent.getActivity(NotificationService.this, 0, intent, 0);

            Intent delIntent = new Intent("tk.aprax.pushreceiver.NOTIFICATION_DELETED");
            delIntent.putExtra("account", account);
            delIntent.putExtra("nid", nid);
            PendingIntent delPendIntent = PendingIntent.getBroadcast(
                    NotificationService.this,
                    Integer.parseInt(nid)*100+accountNumbers.get(account),
                    delIntent,
                    0
            );

            NotificationCompat.Builder builderSummary =
                    new NotificationCompat.Builder(NotificationService.this, messageChannelId)
                            .setSmallIcon(R.drawable.ic_logo)
                            .setGroup(GROUP_KEY_NOTIFICATION + account)
                            .setAutoCancel(true)
                            .setGroupSummary(true);

            NotificationCompat.Builder nBuilder =
                    new NotificationCompat.Builder(NotificationService.this, messageChannelId)
                            .setSmallIcon(type.equals("call")?R.drawable.ic_call:R.drawable.ic_message)
                            .setContentTitle(account)
                            .setContentText(subject.contains("\n")?subject.substring(0, subject.indexOf("\n")):subject)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(subject))
                            .setGroup(GROUP_KEY_NOTIFICATION + account)
                            .setDeleteIntent(delPendIntent)
                            .setAutoCancel(true);
            if (application.equals("spreed")) nBuilder.setContentIntent(pendIntent);

            notificationManager.notify(Integer.parseInt(nid)*100+accountNumbers.get(account), nBuilder.build());
            notificationManager.notify(accountNumbers.get(account), builderSummary.build());
        }

        void processNotification(String account, JSONObject jNotification){
            try {
                String nid = jNotification.getString("notification_id");
                String type = jNotification.getString("object_type");
                String application = jNotification.getString("app");
                //if (!(type.contentEquals("chat") || type.contentEquals("call") || type.contentEquals("room")))
                //    continue;
                String subject = jNotification.getString("subject").replace(" sent you a private message", "\n")
                        + jNotification.getString("message");

                doNotify(application, account, type, subject, nid);
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        // There is some magic here. If nid = "", it will get all of the notifications for the
        // account, otherwise it will get just the notification with that nid.
        void getNotifications(String account, String nid){
            wakeLock.acquire(5 * 1000L); // 5 seconds
            Map<String, String> parameters = new HashMap<>();
            parameters.put("format", "json");

            NextcloudRequest nextcloudRequest = new NextcloudRequest.Builder()
                    .setMethod("GET")
                    .setParameter(parameters)
                    .setUrl("/ocs/v2.php/apps/notifications/api/v2/notifications"+(nid.isEmpty()?"":"/"+nid))
                    .build();

            try {
                SingleSignOnAccount ssoAccount =
                        AccountImporter.getSingleSignOnAccount(NotificationService.this, account);
                NextcloudAPI nextcloudAPI = new NextcloudAPI(NotificationService.this, ssoAccount, new GsonBuilder().create(), callback);

                byte[] databuff = new byte[1024];
                InputStream inputStream = nextcloudAPI.performNetworkRequest(nextcloudRequest);
                int rlen;
                StringBuilder apiresponse = new StringBuilder();
                while ((rlen = inputStream.read(databuff)) >= 0)
                    apiresponse.append(new String(databuff, 0, rlen));
                inputStream.close();

                Log.d(LOG_TAG, "Notifications ("+account+"): "+apiresponse);

                JSONObject jMessage = new JSONObject(apiresponse.toString());

                if (!nid.isEmpty()){
                    processNotification(account, jMessage.getJSONObject("ocs").getJSONObject("data"));
                    return;
                }

                JSONArray jArray = jMessage.getJSONObject("ocs").getJSONArray("data");
                for (int i = 0; i < jArray.length(); i++) {
                    processNotification(account, jArray.getJSONObject(i));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onOpen(EventSource eventSource, Response response) {
            pingtime = System.currentTimeMillis();
            try {
                Log.d(LOG_TAG, "onOpen: " + response.code());
                for (Map.Entry<String, Integer> pair : accountNumbers.entrySet())
                    new Thread(() -> getNotifications(pair.getKey(), "")).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onEvent(EventSource eventSource, @org.jetbrains.annotations.Nullable String id, @org.jetbrains.annotations.Nullable String eventtype, String data) {
            Log.d(LOG_TAG, "New SSE message event=" + eventtype + " message=" + data);
            pingtime = System.currentTimeMillis();
            if (eventtype.equals("warning")){
                wakeLock.acquire(5 * 1000L); // 5 seconds
                NotificationCompat.Builder nBuilder =
                        new NotificationCompat.Builder(NotificationService.this, warningChannelId)
                                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                                .setContentTitle("Push notification WARNING")
                                .setContentText(data)
                                .setStyle(new NotificationCompat.BigTextStyle().bigText(data))
                                .setAutoCancel(true);

                notificationManager.notify(99, nBuilder.build());
            }
            if (!eventtype.equals("notification")) return;
            try {
                JSONObject jMessage = new JSONObject(data);
                String encryptedsubject = jMessage.getString("subject");
                String decryptedSubject = decryptString(encryptedsubject, privateKey);
                Log.d(LOG_TAG, decryptedSubject);

                String deviceIdentifier = jMessage.getString("deviceIdentifier");
                String account = accountIdentifiers.get(deviceIdentifier);
                Log.d(LOG_TAG, "Found account: " + account);

                jMessage = new JSONObject(decryptedSubject);
                String nid = jMessage.getString("nid");
                String application = jMessage.has("app") ? jMessage.getString("app") : "";
                String subject = jMessage.has("subject") ? jMessage.getString("subject") : "";
                String type = jMessage.has("type") ? jMessage.getString("type") : "";
                boolean delete = jMessage.has("delete") && jMessage.getBoolean("delete");

                if (delete){
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    StatusBarNotification[] statusBarNotifications = notificationManager.getActiveNotifications();
                    String groupKey = null;

                    for (StatusBarNotification statusBarNotification : statusBarNotifications) {
                        if (Integer.parseInt(nid)*100+accountNumbers.get(account) == statusBarNotification.getId()) {
                            groupKey = statusBarNotification.getGroupKey();
                            break;
                        }
                    }

                    int counter = 0;
                    for (StatusBarNotification statusBarNotification : statusBarNotifications) {
                        if (statusBarNotification.getGroupKey().equals(groupKey)) {
                            counter++;
                        }
                    }

                    Log.d(LOG_TAG, "Counter: " + counter);
                    notificationManager.cancel(Integer.parseInt(nid)*100+accountNumbers.get(account));
                    if (counter == 2) notificationManager.cancel(accountNumbers.get(account));
                } else if (application.equals("spreed") && type.equals("call"))
                    // calls are highly time sensitive, therefore notify direct rather than pulling details.
                    doNotify(application, account, type, subject, nid);
                else
                    new Thread(() -> getNotifications(account, nid)).start();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onClosed(EventSource eventSource) {
            Log.d(LOG_TAG, "onClosed: " + eventSource.toString());
            if (!networkConnected) return;

            if (gracefulviolence){
                gracefulviolence = false;
                return;
            }
            wakeLock.acquire(5 * 1000L); // 5 seconds
            try {
                Thread.sleep(2000);
            } catch (
                    Exception e) {
                e.printStackTrace();
            }

            factory = EventSources.createFactory(client);
            source = factory.newEventSource(request, listener);
        }

        @Override
        public void onFailure(EventSource eventSource, @org.jetbrains.annotations.Nullable Throwable t, @org.jetbrains.annotations.Nullable Response response) {
            Log.d(LOG_TAG, "onFailure: " + (response != null ? response.code() : "response null"));
            if (!networkConnected) return;

            if (gracefulviolence){
                gracefulviolence = false;
                if (response == null || response.code() != 401) return;
            }
            wakeLock.acquire(5 * 1000L); // 5 seconds
            try {
                Thread.sleep(2000);
            } catch (
                    Exception e) {
                e.printStackTrace();
            }
            if (response != null && response.code() == 401)
                doConnect();
            else {
                factory = EventSources.createFactory(client);
                source = factory.newEventSource(request, listener);
            }
        }
    };
}
